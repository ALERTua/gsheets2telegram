from distutils.util import strtobool

__all_values = dict()


def sheet_all_values(_sheet):
    global __all_values
    if _sheet.id not in __all_values:
        __all_values[_sheet.id] = _sheet.get_all_values()
    return __all_values[_sheet.id]


def strip_str_chars(_str, _chars):
    for _char in _chars:
        _str = _str.replace(_char, '')
    return _str


def str_to_bool(value):
    try:
        return bool(strtobool(value))
    except Exception as e:
        print("Couldn't convert '%s' to bool: %s %s" % (value, type(e), e))
