import typing
from copy import copy

import gspread
from oauth2client.service_account import ServiceAccountCredentials
from telethon.sync import TelegramClient
from telethon.tl.functions.contacts import ImportContactsRequest
from telethon.tl.functions.messages import AddChatUserRequest
from telethon.tl.types import InputPhoneContact

import config
from common import common_tools as tools
from entities.user import GUser

if typing.TYPE_CHECKING:
    pass


def get_creds():
    return ServiceAccountCredentials.from_json_keyfile_name(config.GOOGLE_API_KEY_FILENAME,
                                                            ['https://spreadsheets.google.com/feeds',
                                                             'https://www.googleapis.com/auth/drive',
                                                             'https://www.googleapis.com/auth/spreadsheets'])


def get_users_from_sheet(_sheet):
    all_values = copy(tools.sheet_all_values(_sheet))
    column_header_names = all_values.pop(0)
    cell_phone_column_index = column_header_names.index(config.GOOGLE_SHEETS_CELL_PHONE_COLUMN_NAME)
    telegram_column_index = column_header_names.index(config.GOOGLE_SHEETS_TELEGRAM_COLUMN_NAME)
    authorized_column_index = column_header_names.index(config.GOOGLE_SHEETS_AUTHORIZED_COLUMN_NAME)
    name_column_index = column_header_names.index(config.GOOGLE_SHEETS_NAME_COLUMN_NAME)
    surname_column_index = column_header_names.index(config.GOOGLE_SHEETS_SURNAME_COLUMN_NAME)
    house_column_index = column_header_names.index(config.GOOGLE_SHEETS_HOUSE_COLUMN_NAME)
    section_column_index = column_header_names.index(config.GOOGLE_SHEETS_SECTION_COLUMN_NAME)
    floor_column_index = column_header_names.index(config.GOOGLE_SHEETS_FLOOR_COLUMN_NAME)
    apt_column_index = column_header_names.index(config.GOOGLE_SHEETS_APT_COLUMN_NAME)
    added_column_index = column_header_names.index(config.GOOGLE_SHEETS_ADDED_COLUMN_NAME)

    gusers = list()
    for row_index, row in enumerate(all_values):
        _name = row[name_column_index]
        if not _name:
            continue

        _cellphone = row[cell_phone_column_index]
        _telegram = row[telegram_column_index]
        _authorized = row[authorized_column_index]
        _surname = row[surname_column_index]
        _house = row[house_column_index]
        _section = row[section_column_index]
        _floor = row[floor_column_index]
        _apt = row[apt_column_index]
        _added = row[added_column_index]
        _row = row_index + 2

        _guser = GUser(cellphone=_cellphone, telegram=_telegram, authorized=_authorized, name=_name, surname=_surname,
                       house=_house, section=_section, floor=_floor, apt=_apt, added=_added, row=_row)
        gusers.append(_guser)

    return gusers


def main():
    creds = get_creds()
    gc = gspread.authorize(creds)
    wks = gc.open_by_url(config.GOOGLE_SHEETS_SHEET_URL)
    sheets = wks.worksheets()
    sheet = list(filter(lambda s: config.GOOGLE_SHEET_SHEET_NAME_PART.lower() in s.title.lower(), sheets))
    if not len(sheet):
        raise Exception("no sheet")

    sheet = sheet[0]
    users = get_users_from_sheet(sheet)

    # todo: for future use
    # bot_client = TelegramClient('bot', config.TELEGRAM_API_ID, config.TELEGRAM_API_HASH)
    # bot_client.start(bot_token=config.TELEGRAM_BOT_TOKEN)
    # me = bot_client.get_me()
    # assert me

    real_client = TelegramClient('real', config.TELEGRAM_API_ID, config.TELEGRAM_API_HASH)
    real_client.start(config.MY_PHONE)
    my_entity = real_client.get_entity(config.MY_PHONE)
    chat_id = config.TELEGRAM_VERIFIED_USERS_CHANNEL_ID

    # todo: check among participants
    # chat_entity = real_client.get_entity(chat_id)
    # participants = real_client.get_participants(chat_entity)

    unadded_users = list(filter(lambda u: u.authorized and not u.added, users))
    for guser in unadded_users:
        _name = "{u.name} {u.surname}".format(u=guser)
        _name = _name.strip()
        _surname = "{u.house} {u.section}{u.floor} {u.apt}".format(u=guser)
        _surname = _surname.strip()
        added_contacts = list()
        if guser.telegram:
            added_contacts.append(InputPhoneContact(my_entity.id, guser.telegram, _name, _surname))
        if guser.cellphone:
            added_contacts.append(InputPhoneContact(my_entity.id, guser.cellphone, _name, _surname))
        if not len(added_contacts):
            print("Skipping %s. Neither cellphone nor telegram added." % guser)
            continue

        contacts = real_client(ImportContactsRequest(added_contacts))
        for user in contacts.users:
            result = real_client(AddChatUserRequest(user_id=user, fwd_limit=0, chat_id=chat_id))
            if result:
                # todo: result success check
                guser.mark_added(sheet)


if __name__ == '__main__':
    main()
