from copy import copy

import config
from common import common_tools as tools


class GUser(object):
    def __init__(self,
                 cellphone=None,
                 telegram=None,
                 authorized=None,
                 name=None,
                 surname=None,
                 house=None,
                 section=None,
                 floor=None,
                 apt=None,
                 added=None,
                 row=None
                 ):
        self.cellphone = tools.strip_str_chars(cellphone, '()- ') if cellphone else None
        self.telegram = tools.strip_str_chars(telegram, '@ ') if telegram else None
        self.authorized = tools.str_to_bool(authorized.strip().lower()) if authorized else None
        self.name = name.strip()
        self.surname = surname.strip()
        self.house = house.strip() if house else None
        self.section = section.strip() if section else None
        self.floor = int(floor.strip()) if floor else None
        self.apt = int(apt.strip()) if apt else None
        self.added = tools.str_to_bool(added.strip().lower()) if added else None
        self.row = row

    def __str__(self):
        return "%s %s" % (self.name, self.surname)

    def mark_added(self, _sheet):
        """

        :type _sheet: Worksheet
        """
        all_values = copy(tools.sheet_all_values(_sheet))
        column_header_names = all_values.pop(0)
        added_column_index = column_header_names.index(config.GOOGLE_SHEETS_ADDED_COLUMN_NAME) + 1
        added = _sheet.cell(self.row, added_column_index)
        added_value = tools.str_to_bool(added.value)
        if added_value is False:
            _sheet.update_cell(self.row, added_column_index, 'TRUE')
